import React from 'react';
import './App.css';
import {useSelector,useDispatch} from 'react-redux'
import {isLogged,increment,decrement} from './actions'


function App() {
  const counter = useSelector(state => state.counter)
  const loggedIn = useSelector(state => state.loggedIn)
  const dispatch = useDispatch()

  return (
    <div className="App">
      {loggedIn ?
        <div className="counter">
        <h1>Counter App</h1>
        <h1>{counter}</h1>
        <button onClick={()=>dispatch(increment(1))}> + </button>
        <button onClick={()=>dispatch(decrement(1))}> - </button>
        <br/>
        <button onClick={()=>dispatch(increment(2))}>Double</button>
      </div>
        : <button onClick={()=>dispatch(isLogged())}>Log In</button>
      }




    </div>
  );
}

export default App;
