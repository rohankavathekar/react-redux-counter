import {combineReducers} from 'redux'
import counterReducer from './counter'
import logReducer from './loggedIn'

const rootReducer = combineReducers({
  counter: counterReducer,
  loggedIn: logReducer
})

export default rootReducer